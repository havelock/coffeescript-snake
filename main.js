(function() {
  var BLOCK_SIZE, FIELD_HEIGHT, FIELD_WIDTH, HEIGHT, INITIAL_LENGTH, MOVEMENT_SPEED, WIDTH, animate, canvas, context, g_over, g_place_food, renderSnake, snake, _block_in_snake, _modify_coordinate, _movement_allowed, _ref, _x;
  BLOCK_SIZE = 8;
  WIDTH = 32;
  HEIGHT = 32;
  FIELD_WIDTH = WIDTH * BLOCK_SIZE;
  FIELD_HEIGHT = HEIGHT * BLOCK_SIZE;
  INITIAL_LENGTH = 3;
  MOVEMENT_SPEED = 150;
  snake = {
    score: 0,
    direction: 2,
    blocks: []
  };
  for (_x = _ref = INITIAL_LENGTH - 1; _ref <= 0 ? _x <= 0 : _x >= 0; _ref <= 0 ? _x++ : _x--) {
    snake.blocks.push({
      x: _x,
      y: 0
    });
  }
  document._LAST_KEY = 0;
  document._GAME_PAUSED = false;
  document.onkeydown = function(e) {
    var direction;
    direction = 0;
    switch (e.keyCode) {
      case 38:
        direction = 1;
        break;
      case 39:
        direction = 2;
        break;
      case 40:
        direction = 3;
        break;
      case 37:
        direction = 4;
        break;
      case 32:
        document._GAME_PAUSED = !document._GAME_PAUSED;
    }
    document.getElementById('score').innerHTML = document._GAME_PAUSED ? 'Pause' : snake.score;
    return document._LAST_KEY = direction;
  };
  canvas = document.getElementById('canvas');
  canvas.style.display = 'block';
  canvas.style.width = FIELD_WIDTH + 'px';
  canvas.style.height = FIELD_HEIGHT + 'px';
  canvas.width = FIELD_WIDTH;
  canvas.height = FIELD_HEIGHT;
  canvas.style.border = '2px solid black';
  canvas.style.margin = '100px auto 0';
  context = canvas.getContext('2d');
  _modify_coordinate = function(x, y, direction, delta) {
    switch (direction) {
      case 1:
        y -= delta;
        break;
      case 2:
        x += delta;
        break;
      case 3:
        y += delta;
        break;
      case 4:
        x -= delta;
    }
    return {
      x: x,
      y: y
    };
  };
  _block_in_snake = function(x, y, snake) {
    var block, _i, _len, _ref2;
    _ref2 = snake.blocks;
    for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
      block = _ref2[_i];
      if (block.x === x && block.y === y) {
        return true;
      }
    }
    return false;
  };
  _movement_allowed = function(x, y, snake, canvas) {
    return x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT && !_block_in_snake(x, y, snake);
  };
  window.requestAnimFrame = (function(callback) {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
      return window.setTimeout(callback, 1000 / 60);
    };
  })();
  renderSnake = function(snake, context) {
    var block, _i, _len, _ref2;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = 'black';
    context.lineWidth = 1;
    context.strokeStyle = 'black';
    context.beginPath();
    _ref2 = snake.blocks;
    for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
      block = _ref2[_i];
      context.rect(block.x * BLOCK_SIZE, block.y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
    }
    context.fill();
    context.stroke();
    context.beginPath();
    context.arc((snake.food.x + 0.5) * BLOCK_SIZE, (snake.food.y + 0.5) * BLOCK_SIZE, BLOCK_SIZE / 2, 0, 2 * Math.PI, false);
    context.fill();
    return context.stroke();
  };
  animate = function(snake, context, canvas, lastframetime) {
    var movementDelta, new_snake_head, time, timeDelta;
    time = (new Date()).getTime();
    timeDelta = time - lastframetime;
    movementDelta = parseInt(timeDelta / MOVEMENT_SPEED);
    if (document._GAME_PAUSED || movementDelta < 1) {
      return window.requestAnimFrame(function() {
        return animate(snake, context, canvas, lastframetime);
      });
    }
    if (document._LAST_KEY) {
      if (Math.abs(snake.direction - document._LAST_KEY) !== 2) {
        snake.direction = document._LAST_KEY;
      }
      document._LAST_KEY = 0;
    }
    new_snake_head = _modify_coordinate(snake.blocks[0].x, snake.blocks[0].y, snake.direction, 1);
    if (!_movement_allowed(new_snake_head.x, new_snake_head.y, snake, canvas)) {
      return g_over(snake, context, canvas);
    }
    snake.blocks.unshift(new_snake_head);
    if (snake.blocks[0].x === snake.food.x && snake.blocks[0].y === snake.food.y) {
      snake.score += 1;
      document.getElementById('score').innerHTML = snake.score;
      g_place_food(snake);
    } else {
      snake.blocks.pop();
    }
    renderSnake(snake, context);
    return window.requestAnimFrame(function() {
      return animate(snake, context, canvas, time);
    });
  };
  g_over = function(snake, context, canvas) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    window.requestAnimFrame = null;
    return document.getElementById('score').innerHTML = 'Game over';
  };
  g_place_food = function(snake) {
    var x, y;
    x = snake.blocks[0].x;
    y = snake.blocks[0].y;
    while (_block_in_snake(x, y, snake)) {
      x = parseInt(Math.floor(Math.random() * WIDTH));
      y = parseInt(Math.floor(Math.random() * HEIGHT));
    }
    return snake.food = {
      x: x,
      y: y
    };
  };
  g_place_food(snake);
  renderSnake(snake, context);
  setTimeout(function() {
    var startTime;
    startTime = (new Date()).getTime();
    return animate(snake, context, canvas, startTime);
  }, 500);
}).call(this);
