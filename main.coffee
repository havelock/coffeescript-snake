BLOCK_SIZE = 8
WIDTH = 32 # in blocks
HEIGHT = 32
FIELD_WIDTH = WIDTH * BLOCK_SIZE
FIELD_HEIGHT = HEIGHT * BLOCK_SIZE
INITIAL_LENGTH = 3 # square blocks of BLOCK_SIZExBLOCK_SIZE px
MOVEMENT_SPEED = 150 #  ms to move 1block

snake =
    score: 0
    direction: 2 # of the head
    blocks: []
snake.blocks.push({x: _x, y: 0}) for _x in [(INITIAL_LENGTH - 1)..0]

document._LAST_KEY = 0
document._GAME_PAUSED = false
document.onkeydown = (e) ->
    direction = 0
    switch e.keyCode
        when 38
            direction = 1
        when 39
            direction = 2
        when 40
            direction = 3
        when 37
            direction = 4
        when 32
            document._GAME_PAUSED = not document._GAME_PAUSED        
    document.getElementById('score').innerHTML = if document._GAME_PAUSED then 'Pause' else snake.score
    document._LAST_KEY = direction

canvas = document.getElementById('canvas')

canvas.style.display = 'block'
canvas.style.width = FIELD_WIDTH + 'px'
canvas.style.height = FIELD_HEIGHT + 'px'
canvas.width = FIELD_WIDTH
canvas.height = FIELD_HEIGHT
canvas.style.border = '2px solid black'
canvas.style.margin = '100px auto 0'

context = canvas.getContext('2d')

_modify_coordinate = (x, y, direction, delta) ->
    switch direction
        when 1
            y -= delta
        when 2
            x += delta
        when 3
            y += delta
        when 4
            x -= delta

    return {x: x, y: y}

_block_in_snake = (x, y, snake) ->
    for block in snake.blocks
        if block.x == x and block.y == y
            return true
    return false

_movement_allowed = (x, y, snake, canvas) ->
    x >= 0 and y >= 0 and x < WIDTH and y < HEIGHT and not _block_in_snake(x, y, snake)

window.requestAnimFrame = ((callback) ->
    window.requestAnimationFrame or window.webkitRequestAnimationFrame or window.mozRequestAnimationFrame or window.oRequestAnimationFrame or window.msRequestAnimationFrame or (callback) ->
        window.setTimeout(callback, 1000 / 60)
    )()

renderSnake = (snake, context) ->
    context.clearRect(0, 0, canvas.width, canvas.height)

    context.fillStyle = 'black'
    context.lineWidth = 1
    context.strokeStyle = 'black'
    
    context.beginPath()
    context.rect(block.x * BLOCK_SIZE, block.y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE) for block in snake.blocks
    context.fill()
    context.stroke()
    context.beginPath()
    context.arc((snake.food.x + 0.5) * BLOCK_SIZE, (snake.food.y + 0.5) * BLOCK_SIZE, BLOCK_SIZE / 2, 0, 2 * Math.PI, false)
    context.fill()
    context.stroke()

animate = (snake, context, canvas, lastframetime) ->
    time = (new Date()).getTime()
    timeDelta = time - lastframetime
    movementDelta = parseInt(timeDelta / MOVEMENT_SPEED)
    if document._GAME_PAUSED or movementDelta < 1
        return window.requestAnimFrame(() ->
            animate(snake, context, canvas, lastframetime)
        )        

    if document._LAST_KEY
        if Math.abs(snake.direction - document._LAST_KEY) != 2 # 1 excludes 3, 2 excludes 4, ...
            snake.direction = document._LAST_KEY
        document._LAST_KEY = 0

    new_snake_head = _modify_coordinate(snake.blocks[0].x, snake.blocks[0].y, snake.direction, 1)

    if not _movement_allowed(new_snake_head.x, new_snake_head.y, snake, canvas)
        return g_over(snake, context, canvas)

    snake.blocks.unshift(new_snake_head)
    if snake.blocks[0].x == snake.food.x and snake.blocks[0].y == snake.food.y
        snake.score += 1
        document.getElementById('score').innerHTML = snake.score
        g_place_food(snake)
    else
        snake.blocks.pop()

    renderSnake(snake, context)

    window.requestAnimFrame(() ->
        animate(snake, context, canvas, time)
    )

g_over = (snake, context, canvas) ->
    context.clearRect(0, 0, canvas.width, canvas.height)
    window.requestAnimFrame = null
    document.getElementById('score').innerHTML = 'Game over'

g_place_food = (snake) ->
    x = snake.blocks[0].x
    y = snake.blocks[0].y
    while _block_in_snake(x, y, snake)
        x = parseInt(Math.floor(Math.random() * WIDTH))
        y = parseInt(Math.floor(Math.random() * HEIGHT))
    snake.food = {x: x, y: y}

g_place_food(snake)

renderSnake(snake, context)

setTimeout(() ->
    startTime = (new Date()).getTime()
    animate(snake, context, canvas, startTime)
, 500)